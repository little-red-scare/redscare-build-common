# witchpixels' gitlab ci dumping ground 

[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Just the common routines I use in my projects that you might want to use as
well.

If you don't want to have to contend with any changes I might make, feel free to
fork this.

## Usage

All you need to do is include a few lines in your `.gitlab-ci.yml`. The
Gitversion hook uses `.pre` step so it will always run.

### On gitlab.com

Add the following to your `.gitlab-ci.yml`:

``` yaml
include: 
    - project: witchpixels/wpx-build-common
      ref: main
      file: gitlab-extensions/gitversion-ci-cd-plugin-extension.gitlab-ci.yml

determine-version:
  extends: .gitversion_function
```

Change main to a release tag if you want to pin the version for project
stability.

### On your own gitlab instance

Add the following to your `.gitlab-ci.yml`:

``` yaml
include: https://gitlab.com/witchpixels/wpx-build-common/-/raw/main/gitlab-extensions/gitversion-ci-cd-plugin-extension.gitlab-ci.yml

determine-version:
  extends: .gitversion_function
```

Change `main` in the url to a release tag if you want to pin the version for
project stability.



## Contributing

I welcome issues and pull requests for improvements, just be polite and patient,
I'm just one person with a crushing anxiety disorder <3
